---
title: About INA
date: 2020-05-25
---

INA is an acronym that summarizes this project in three Latin words: **I**ntelligente, **N**untium, **A**ffectus (or Understand, News, Feelings).

This project has been brewing in my mind ever since I have been introduced to Aspect-Based Sentiment Analysis in an interview for a Data Scientist internship position.
Later, for my Natural Language Processing course in University we were tasked to come up with some problem and attempt to solve it. So, I thought this to be a brilliant opportunity to carry out a project that had been nesting in my mind. And thus, INA was born.

The project resides [here](https://gitlab.com/p-skaisgiris/ina).

<br /> <br />

Ina is a also a female Lithuanian name.
